# projecttemplate-sb-mvn

This is a Spring Boot Template that supports MySQL, Apache Tiles, and Hibernate (QueryDSL). Be sure to update /src/main/resources/application.properties to point to your database URL. Also update your database username and password.