package com.oreillyauto.service.impl;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.TestRepository;
import com.oreillyauto.service.TestService;

@Service("testService")
public class TestServiceImpl implements TestService {
	@Autowired
	TestRepository testRepo;

	@Override
	public Timestamp getDatabaseTimestamp() {
		return testRepo.getDatabaseTimestamp();
	}
	
}
