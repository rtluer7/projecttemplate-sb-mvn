package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjecttemplateSbMvnApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjecttemplateSbMvnApplication.class, args);
	}

}
